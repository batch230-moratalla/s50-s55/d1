import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom';
import   Swal  from 'sweetalert2'

import React from 'react'

const Register = () => {
    const { user } = useContext(UserContext);
    const navigate = useNavigate()
    const [ firstName, setFirstName ] = useState('')
    const [ lastName, setLastName ] = useState('')
    const [ email, setEmail ] = useState('')
    const [ password1, setPassword1 ] = useState('')
    const [ password2, setPassword2 ] = useState('');
    const [ mobileNumber, setMobileNumber ] = useState('')
    const [ createon, setCreateon ] = useState(new Date())
    
    const [isActive, setIsActive] = useState(false);
    // Check if values are successfully binded
    //  console.log(email);
    //  console.log(password1);
    //  console.log(password2);



    function registerUser (event){
        event.preventDefault()
        fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
            method: "POST",
            headers: { 'Content-Type' : "application/json"},
            body: JSON.stringify({
                firstName: firstName,
                lastName : lastName,
                email: email,
                password: password1,
                mobileNumber: mobileNumber
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            if(data) {
                Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
                setEmail('');
                setPassword1('');
                setPassword2('');
                navigate("/login")

            } else {
                Swal.fire({
                    title: "Registration Failed",
                    icon: "error",
                    text: "Please check your password"
                })
            }
        })
        // alert('Login Successful!')

        // Clear input field
        // alert('Thank you for registering!')
    }
    useEffect(() => {
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password1, password2])


    return (
    
        (user.id !== null)
        ?
            <navigate to="/courses" />
        :
        <Form onSubmit={(event) => registerUser(event)}>
            <h1>Register</h1>
            <Form.Group controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="First Name"
                    value={firstName}
                    onChange={event => setFirstName(event.target.value)} 
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>
            <Form.Group controlId="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Last Name"
                    value={lastName}
                    onChange={event => setLastName(event.target.value)} 
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={event => setEmail(event.target.value)} 
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Mobile Number"
                    value={mobileNumber}
                    onChange={event => setMobileNumber(event.target.value)} 
                    required
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password1}
                    onChange={event => setPassword1(event.target.value)} 
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password"
                    value={password2}
                    onChange={event => setPassword2(event.target.value)} 
                    required
                />
            </Form.Group>
        
            { isActive ? 
                <Button variant="primary" type="submit" id="submitBtn" >
                    Register
                </Button>
            :
                <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Register
                </Button>
            }
            </Form>
    )
}

export default Register

	

/* 
original code b4 activity
import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function Register(){

	const {user} = useContext(UserContext);

    //State hooks to store the values of input fields
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // Check if values are successfully binded
    console.log(email);
    console.log(password1);
    console.log(password2);

    function registerUser(event){

        // Prevents page redirection via form submission
        event.preventDefault();

        // Clear input fields
        setEmail('');
        setPassword1('');
        setPassword2('');
        alert('Thank you for registering');
    }

    useEffect(() => {
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, password1, password2])

    // const { user } = useContext(UserContext);
    // const { user, setUser } = useContext(UserContext);

    return(
        (user.id !== null)
        ?
        <Navigate to='/courses'/> 
        :
        <Form onSubmit={(event) => registerUser(event)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value = {email}
                    onChange = {event => setEmail(event.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value = {password1}
                    onChange = {event => setPassword1(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value = {password2}
                    onChange = {event => setPassword2(event.target.value)}
                    required
                />
            </Form.Group>

            <div className='pt-3'>
                { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
                }
            </div>
        </Form>
    )
}
*/

/* 
{ if(isActive == true) {
  <Button variant="primary" type="submit" id="submitBtn" disabled>
  Submit
</Button>
}
  
}
else
<Button variant="primary" type="submit" id="submitBtn" disabled>
  Submit
</Button> */

/* 
return(
    (user.email !== null)
        ?
            <Navigate to="/courses" />
        :

    <Form onSubmit={(event) => registerUser(event)}>
    <h3>Register</h3>
      <Form.Group controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control 
            type="email" 
            placeholder="Enter email"
            value = {email}
            onChange = {event => setEmail(event.target.value)}
            required
          />
          <Form.Text className="text-muted">
              We'll never share your email with anyone else.
          </Form.Text>
      </Form.Group>

      <Form.Group controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control 
            type="password" 
            placeholder="Password" 
            value={password1}
            onChange={e => setPassword1(e.target.value)}
            required
          />
      </Form.Group>

      <Form.Group controlId="password2">
          <Form.Label>Verify Password</Form.Label>
          <Form.Control 
            type="password" 
            placeholder="Verify Password" 
            value={password2}
            onChange={e => setPassword2(e.target.value)}
            required
          />
      </Form.Group>
    
      {  isActive ? //true
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
        : // false
        <Button variant="secondary" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      }
    </Form>
  )

*/

/* 
from sir topher

import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function Register(){

	const {user} = useContext(UserContext);

    //State hooks to store the values of input fields
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // Check if values are successfully binded
    console.log(email);
    console.log(password1);
    console.log(password2);

    function registerUser(event){

        // Prevents page redirection via form submission
        event.preventDefault();

        // Clear input fields
        setEmail('');
        setPassword1('');
        setPassword2('');
        alert('Thank you for registering');
    }

    useEffect(() => {
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, password1, password2])

    // const { user, setUser } = useContext(UserContext);

    return(
        (user.id !== null)?
        <Navigate to ="/courses" />
        :
        <Form onSubmit={(event) => registerUser(event)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value = {email}
                    onChange = {event => setEmail(event.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value = {password1}
                    onChange = {event => setPassword1(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value = {password2}
                    onChange = {event => setPassword2(event.target.value)}
                    required
                />
            </Form.Group>

            <div className='pt-3'>
                { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
                }
            </div>
        </Form>
    )
}

*/

/* 
My Codes
import { Form, Button } from "react-bootstrap";
// import { useState, useEffect } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext, useState, Fragment, useEffect } from "react";

// useState is React Hook that allows you to add state to a functional component. It returns an array with two values: the current state and a function to update it. The Hook takes an initial state value as an argument and returns an updated state value whenever the setter function is called.
export default function Register (){
  const  { user } = useContext(UserContext);

  // const { user, setUser } = useContext(UserContext)
  // State hooks to store the values of the input fields
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  // check if values are successfully binded
  console.log(email);
  console.log(password1);
  console.log(password2);

  function registerUser(event){
    // Prevents page redirection via form submission
    event.preventDefault();
    // Clear input fields
    setEmail('');
    setPassword1('');
    setPassword2('');
    alert('Thank you for registering');
  }
  useEffect(() => {
    if((email !== "" && password1 !== '' && password2 !== "") && (password1 === password2)){
      setIsActive(true);
    }
    else{
      setIsActive(false);
    }
  }, [email, password1, password2]) 
  // gagana lang si use effect pag may nagbago kay email, password1, and password 2 with the uise of []
  return(
    (user.id !== null)
    ?
        <Navigate to="/courses"/>
    :
    <Form onSubmit={(event) => registerUser(event)}>
    <h3>Register</h3>
      <Form.Group controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control 
            type="email" 
            placeholder="Enter email"
            value = {email}
            onChange = {event => setEmail(event.target.value)}
            required
          />
          <Form.Text className="text-muted">
              We'll never share your email with anyone else.
          </Form.Text>
      </Form.Group>

      <Form.Group controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control 
            type="password" 
            placeholder="Password" 
            value={password1}
            onChange={e => setPassword1(e.target.value)}
            required
          />
      </Form.Group>

      <Form.Group controlId="password2">
          <Form.Label>Verify Password</Form.Label>
          <Form.Control 
            type="password" 
            placeholder="Verify Password" 
            value={password2}
            onChange={e => setPassword2(e.target.value)}
            required
          />
      </Form.Group>
      /* ternary operator *
      {  isActive ? 
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
        : // false
        <Button variant="secondary" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      }
    </Form>
  )
}
*/