import React from 'react';
import { Container, } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';


export default function PageNotFound(){
  return(
    <div>
      <div className='text-center'>
        <div>
        <br/>
        <br/>
        <br/>
        <h1>Opps!</h1>
        <br/>
        <br/>
        </div>
        <h2 className='text-danger'>Error 404</h2>
        <h3>Page not Found!</h3>
        <p>Something went Wrong and the page you're looking for cannot be found</p><br/>
        <p>Please visit our <Link as={NavLink} to="/">Homepage</Link> or use the NavBar</p>
      </div>
    </div>
  )
}