import React, { useEffect, useState } from 'react';
import { Fragment } from 'react';
import coursesData from "../data/coursesData"
import CourseCard from '../components/CourseCard';

import { Navigate } from "react-router-dom"
import { useContext } from "react"
import UserContext from "../UserContext"

export default function Courses(){
/*   
  console.log("Contents of coursesData")
  console.log(coursesData)
  console.log(coursesData[0])
  let count = 0;
 */
  const { user } = useContext(UserContext)
  // const { courses, setCourses } = useState([])
  const [ courses, setCourses ]  = useState([])

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/`)
    .then(result => result.json())
    .then(data => {
      console.log(data);
      setCourses(data.map(course => {
        return (
          <CourseCard key={course.id} courseProp = {course} />
        )
      }))
    })
  }, [])
/* 
  const courses = coursesData.map(course => {
    return (
      <CourseCard key={course.id} courseProp = {course} />
    )
  })
  const { user, setUser } = useContext(UserContext);
 */
  return (
    
    // (user.email !== null)
    (user.isAdmin)
    ?
      <Navigate to='/admin'/>
    :
    <>
      <h1> Courses</h1>
      {courses}
    </>
  )
}

/* 

export default function Courses(){
  console.log("Contents of coursesData")
  console.log(coursesData)
  console.log(coursesData[0])

  // array method to display all courses
  let count = 0;
  const courses = coursesData.map(course => {
    return (
      <CourseCard key={course.id} courseProp = {course} />
    )
  })
  // return (
  //   <Fragment>
  //     <CourseCard courseProp={coursesData[0]}/>
  //   </Fragment>
  // )
  return (
    <Fragment>
      {courses}
    </Fragment>
  )
}

    // (user.email !== null)
    (user.isAdmin)
    ?
      <Navigate
    <Fragment>
      {courses}
    </Fragment>
    :
    <Navigate to="/register"/>
  )
}


*/