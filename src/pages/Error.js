import Banner from '../components/Banner'

export default function Error(){
  // We created an obkect variable name 'data' to contains the properties we want to sent in the 
  const data ={
    title: '404 - Not found',
    content: 'The page you are looking for cannot be found',
    label: 'Back home'
  }
  return (
    <Banner data={data}/>
  )
}