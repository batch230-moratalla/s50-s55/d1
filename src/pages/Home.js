import { Fragment } from "react";
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
// import Course from "../components/Course"

const data ={
  title: 'Zuitt Coding Bootcamp',
  content: 'Opportunities for everyone, everywhere.',
  destination: '/',
  label: 'Enroll Now'
}


export default function Home(){
  return (
    <Fragment>
      <Banner data={data}/>
      <Highlights />
    </Fragment>
  )
}

/* 
data={data} para gumana kelangan nating ipass ung data using invocation
<Banner /> to <Banner data={data}/>
*/

/* 
Specific Instructions:
1. Create a CourseCard component showing a particular course with the name, description and price inside a React-Bootstrap Card:
- The course name should be in the card title.
- The description and price should be in the card body.
Note: You may refer to this link how to create a Card:
https://react-bootstrap.netlify.app/components/cards/#rb-docs-content

2. Render the CourseCard component in the Home page.
3. Create a git repository named S50.
4. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code - S50.
5. Add the link in Boodle.
*/