import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
import Swal from 'sweetalert2';

export default function Login (){

  // 3 - use state
  // const { user, setUser } = useContext(UserContext); // s45
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setpassword] = useState('');
  const [isActive, setIsActive] = useState(true);

  console.log(email);
  console.log(password);

/* 'http://localhost:4000/users/login'
  function authenticate(event){
    event.preventDefault();

    localStorage.setItem('email', email);
    // email natin sa local storage ilalagay natin kay context
    setUser({
      email: localStorage.getItem('email')
    })

    setEmail('');
    setpassword('');
    // alert('Welcome! You are now Login.');
    console.log(`${email} Welcome Back!`)
    alert("Successfully logged in.")

  }
   */
  function authenticate(event) {
    event.preventDefault()
    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
      headers: {"Content-type": 'application/json'},
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(result => result.json())
    .then(data => {
      console.log(data);
      console.log('Check Accesstoken');
      console.log(data.accessToken);
      // retrieveUserDetails(data.accessToken);

      if(typeof data.accessToken !== "undefined"){
        // localStorage.getItem('token', data.accessToken)
        localStorage.setItem('token', data.accessToken)
        // localStorage.setItem('email', email) 
        // localStorage.setItem('id', user.id) 
        retrieveUserDetails(data.accessToken);
        Swal.fire({
          title: 'Login Successful',
          icon: 'success',
          text: "Welcome to Zuitt!"
        })
      }
      else {
        Swal.fire({
          title: 'Authentication failed',
          icon: 'error',
          text: "Check your login details and try again."
        })
      }
    })
    setEmail('')
  } 

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers:{
        Authorization: `Bearer ${token}`
      }
    })
    .then(result => result.json())
    .then(data => {
      console.log(data);
      setUser({
        id:data._id,
        // email: data.email,
        isAdmin:data.isAdmin
      })
    })
  }

  useEffect(() => {
    if(email !== "" && password !== ''){
      setIsActive(true);
    }
    else{
      setIsActive(false);
    }
  }, [email, password,]) 

  return(
    (user.id !== null)
    // (user.email !== null)
    //conditional for login
    ? 
    //true -means email field is successfully set
    // <Navigate to='/'/>
    <Navigate to='/courses'/> 
    : //false -means email field is not successfully set
    // <Form onSubmit={(e) => login(e)}></Form>
    <div className="container-xxl">
    <Form className="login-page col-sm-8 col-lg-4 mx-auto" onSubmit={(event) => authenticate(event)}>
     {/*  align-items-center justify-center */}
    <h3>Login</h3>
      <Form.Group controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control 
            type="email" 
            placeholder="Enter email"
            value = {email}
            onChange = {event => setEmail(event.target.value)}
            required
          />
          {/* <Form.Text className="text-muted">
              We'll never share your email with anyone else.
          </Form.Text> */}
      </Form.Group>

      <Form.Group controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control 
            type="password" 
            placeholder="Password" 
            value={password}
            onChange={e => setpassword(e.target.value)}
            required
          />
      </Form.Group>
      {/* ternary operator */}
      {  isActive ? //true
        <Button variant="success" type="submit" id="submitBtn">
          Submit
        </Button>
        : // false
        <Button variant="secondary" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      }
    </Form>
    </div>
  )
}

/*    
          (user.email !== null)
          //conditional for login
          ? 
          //true -means email field is successfully set
          <Navigate to='/'/>
          //<Navigate to='/courses'/> 
          : //false -means email field is not successfully set
          // <Form onSubmit={(e) => login(e)}></Form>
  */