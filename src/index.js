import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/css/bootstrap.min.css';
// Removed
// import './index.css';

// Removed
// import reportWebVitals from './reportWebVitals';

// You may check index.html of public folder to identify who is 'root'
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // "React.StrictMode" - similar to error handler
  <React.StrictMode>
    <App />
  </React.StrictMode> 
);

// const name = 'John Smith';
// const user = {
//   firstName: 'jane',
//   lastName: 'Smith'
// }

// function formatName(parameterUser){
//   return parameterUser.firstName + ' ' + parameterUser.lastName;
// }

// const element = <h1>Hello, {formatName(user)}</h1>

// // We need to create a 'root' to display react components
// // Reference: https://beta.reactjs.org/reference/react-dom/client/createRoot
// const root = ReactDOM.createRoot(document.getElementById("root"));
// root.render(element)

/* 
/* 
  React.js - Components Driven Development
    React.js - is an open source front end javascript library for building user interfaces based
  
  What Problem Does it solved?
    Application that require frequent and rapid changes in page data are resouce intensive and complicated to code
  
1. npm install -g create-react-app - installing react
  npx create-react-app react-booking - if the first one does not work
2. create-react-app react-booking

option 1 preparations:
  - npx create-react-app react-booking

option 2 preparations - global state, you dont need to install it on every folder because its accessible from other programs
  - npm install -g create-react-app
  - create-react-app react-booking
  - npm install react-bootstrap bootstrap
*/

/* 
s50 - s55 pasok ung reack folder
from src folder - remove this files
    App.test.js
		index.css
		logo.svg
		reportWebVitals.js

from src folder > index.js remove 
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

// import './index.css';
// import reportWebVitals from './reportWebVitals';

-npm start


for sublime press ctrl shift p > install > package control install package > babel
npm install sweetalert2 swal

*/

/* 
Specific Instructions:
1. Create a CourseCard component showing a particular course with the name, description and price inside a React-Bootstrap Card:
- The course name should be in the card title.
- The description and price should be in the card body.
Note: You may refer to this link how to create a Card:
https://react-bootstrap.netlify.app/components/cards/#rb-docs-content

2. Render the CourseCard component in the Home page.
3. Create a git repository named S50.
4. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code - S50.
5. Add the link in Boodle.

*/

