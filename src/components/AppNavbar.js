// import Container 
// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';

import { Container, Navbar, Nav} from 'react-bootstrap'
import {Link, NavLink} from "react-router-dom";
import { Fragment, useContext, useState } from 'react';
import UserContext from '../UserContext';



// https://react-bootstrap.github.io/components/alerts/

// exported funtion that can be accessible to all

export default function AppNavbar(){
  const  { user }  = useContext(UserContext)
  // const [user, setUser] = useState(localStorage.getItem('email'));
  console.log(user)

  return(
    <Navbar bg="light" expand="lg">
      <Container fluid className='bg-primary'>{/* fixed-top */}
        <Navbar.Brand as={Link} to="/" >Zuitt Booking (from AppNavbar.js)</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto ms-auto">
              <Nav.Link as={NavLink} to="/" href="#home">Home</Nav.Link>
              <Nav.Link as={NavLink} to="/courses" href="#courses">Courses</Nav.Link>
              {(user.id!== null)?
                <Nav.Link as={NavLink} to="/logout" href="#register">Logout</Nav.Link>
                :
                  <Fragment> {/* hindi gagana pag multiple element you need to use Fragment */}
                    <Nav.Link as={NavLink} to="/login" href="#login">Login</Nav.Link> 
                    <Nav.Link as={NavLink} to="/register" href="#register">Register</Nav.Link>
                    {/* <Nav.Link as={NavLink} to="/settings" href="#register">Checkout</Nav.Link> */}
                    {/* gawing icon si checkout  */}
                  </Fragment>
              } 
            </Nav>
          </Navbar.Collapse>
      </Container>
		</Navbar>
  )
}



