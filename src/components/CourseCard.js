import Card from 'react-bootstrap/Card';
import {Button, Row, Col} from 'react-bootstrap';
import { useEffect, useState } from 'react';
import PropTypes from "prop-types";
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {
  // more destructuring
  // const {name, description, price} = courseProp;
  // State Hooks (useState) - a way to store information within a component and trach this information
      // Getter, Setter
      // Variable, function to change the value of a variable
  // const [count, setCount] = useState(0); // count = 0; pag priness nya ung count mag increment xa, +1 + 2 +3
  // const [seats, setSeats] = useState(30);
  // const
  
  // function enroll(){
    /*  if(seat <= 0){
      return alert('No more available slot!')
    }
    else {
      setCount(count + 1)
      seatSlot(seat - 1)
    } */
    
    /*  if(seats > 0){
      setCount(count + 1);
      console.log('Enrollees: ' + count);
      seatSlot(seats - 1);
      console.log('Seats: ' + seats)
    }
    else{
      alert('No more seats available');
    } */
  //   setCount(count + 1);
  //   console.log('Enrollees: ' + count);
  //   setSeats(seats - 1);
  //   console.log('Seats: ' + seats)
  // }
  // useEffect() = always runs the task on the initial render and/or every render (when the state changes in a components)
  // Initial render is when the component is run or displayed for the first time

  // useEffect(() => {
  //   if(seats === 0 ){
  //     alert('No more seats available')
  //   }
  // },[seats]);
  const {_id, name, description, price } = courseProp;

  return (
    // "{}" object destructuring hahaha
    <Card>  {/* '18rem' */}{/* <Card style={{ width: '100%' }}></Card> */}
    <Card.Body>
    {/* since object na si props we can use "." tuldok notation */}
      <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle> 
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle> 
        <Card.Text>{price}</Card.Text>
        <br/>
      {/* <Button variant="primary" onClick={enroll}>Enroll</Button> */}
      <Button as={Link} to={`/courses/${_id}`}>Enroll</Button>
      {/* <Card.Text>Total Enrolled: {count} <br/> Seats Avalable: {seats}</Card.Text> */}
    </Card.Body>
  </Card>
  );
}
  // export default function CourseCard({courseProp}) { 
    
/* 
<Card.Title>Sample Courses</Card.Title>
        <Card.Subtitle className="text-dark">Description:</Card.Subtitle> 
        <Card.Text className="text-dark">This is a sample course offering.</Card.Text>
        {/* <Card.Subtitle className="text-dark">This is a sample course offering.</Card.Subtitle> }
        <Card.Subtitle className="text-dark">Price:</Card.Subtitle> 
        <Card.Text className="text-dark">PhP 40,000</Card.Text>
        {/* <Card.Subtitle className="text-dark">PhP 40,000</Card.Subtitle>  }
        <br/>
      <Button variant="primary">Enroll</Button>
*/

// Check if the CourseCard component is getting the correct property types
/* 
CourseCard.propTypes = {
  courseProp: PropTypes.shape({
    name:PropTypes.string.isRequired,
    description:PropTypes.string.isRequired,
    price:PropTypes.number.isRequired
  })
}

 */
