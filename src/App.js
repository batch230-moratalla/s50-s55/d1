
import './App.css';
import { Container } from 'react-bootstrap';
import {Route, Routes} from 'react-router-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import PageNotFound from './pages/PageNotFound';
import Logout from './pages/Logout';
import { useState } from 'react';
// import { PageNotFound } from './pages/PageNotFound';
import Error from '../src/pages/Error'
import { UserProvider } from './UserContext';
import Settings from './pages/Setting';
import CourseView from './pages/CourseView';

/* 
create component >> configure app.js
*/

// import { UserProvider } from './UserContext';
// import {Use}

function App() {

  // 1 Create state
  const [ user, setUser ] = useState({
    email:localStorage.getItem('email')
    // email:null,
    // id:null
  })
  const unsetUser = () =>{
    localStorage.clear()
  }
  
  return (
    // 2 shinare nya ung state / provide to other components
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <Container fluid>
          <AppNavbar/>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/courses" element={<Courses/>} />
            <Route path='/courses/:courseId' element={<CourseView/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/settings" element={<Settings/>} />
            <Route path="*" element={<Error/>} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  )
};
export default App; 

// To Create a route

/* Syntax:
  <Router>
    <Routes>
      <Route path="/yourDesiredPath" element { <Component />} />
    </Routes>
  </Router> */

  /* 
  gumawa tau ng state na maaring nya maishare sa mga content using UserProvider tab
  - ang trabaho ni useContext, ang mga shinare natin ni UserProvider, pwede nating gamiti tru array deconstructure
  */
